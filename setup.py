import sys

from pybind11 import get_cmake_dir
from pybind11.setup_helpers import Pybind11Extension, build_ext
from setuptools import setup

__version__ = "0.0.1"

ext_modules = [
    Pybind11Extension("bisr_dpw_cpp_routines",
        ["src/cpp_routines.cpp"],
        # Example: passing in the version to the compiled code
        define_macros = [('VERSION_INFO', __version__)],
        ),
]

setup(
    name="bisr_dpw",
    version=__version__,
    install_requires=["networkx",],
    packages=["bisr_dpw"],
    cmdclass={"build_ext":build_ext},
    ext_modules=ext_modules
)
