# BISR-DPW

This is the repository hosting the code used for the benchmarks of https://hal.inria.fr/hal-03272963v1

In particular, it contains an implementation of https://link.springer.com/chapter/10.1007/978-3-642-25870-1_30,
an **XP algorithm for directed pathwidth** ($`O(n^{k+2})`$-time, $`O(n^{k+1})`$-space),
with the slight modification presented in the Appendix of https://hal.inria.fr/hal-03272963v1.

The other algorithm it contains is an **XP algorithm for bipartite independent set reconfiguration**. It may also
be used for **directed pathwidth**, thanks to the **equivalence** showed in https://hal.inria.fr/hal-03272963v1.
Its complexity is $`O(n^{2k+2.5})`$-time and $`O(n^{2})`$-space.

## Installation instruction.

Start by **cloning this repository**.

Then, this should work on all platforms:

    python3 -m build 
    pip install .

To launch the tests:

    python3 -m pytest tests/

## Source code documentation

A documentation of the source code, including the **directed pathwidth solver**
is available at https://bmarchand-perso.gitlab.io/bisr-dpw/.
